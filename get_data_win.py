from google.cloud import bigquery
import pandas as pd
from datetime import datetime, timedelta
import os
from jinja2 import Environment, FileSystemLoader
from weasyprint import HTML

yesterday = (datetime.now() - timedelta(days=1)).strftime("%Y%m%d")
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = 'C:\\Users\\MSI\\Documents\\Credentials\\cder.json'
query = """
SELECT * FROM `id-bigquery-workspace.report.booking_nopurchases`
"""
project = 'id-bigquery-workspace'
path = 'data\\'
pth = 'pdf\\'

def get_data(project,query,filename,path):
    while True:
        folder = filename.lower()
        if not os.path.exists((folder) + ".csv"):
            namafile = folder
        break
    else :
        print("Name has been taken, ERORR")
    print('Saved with Name :' ,namafile +'.csv')
    bigquery_client = bigquery.Client(project=project)
    print('Processing')
    data = pd.read_gbq(query,project_id=project,dialect='standard')
    data.to_csv(os.path.join(path,namafile+r'_'+yesterday+r'.csv'))
    #data.to_csv(path=path+namafile+'_'+today+'.csv')
    # data.to_csv(namafile +'_'+today+'.csv')
    print('Done')

def to_pdf(namafile,pdf):
    data = pd.read_csv(r'data/' + namafile+r'_'+yesterday+r'.csv')
    data = data[['booking_date','booking_time','booking_location','customer_name','make','model','transmission','trim','year']]
    data.index += 1 
    env = Environment(loader=FileSystemLoader('.'))
    template = env.get_template("myreport.html")
    template_vars = {"title" : "Report",
                "table": data.to_html()}
    html_out = template.render(template_vars)
    HTML(string=html_out).write_pdf(os.path.join(pth,pdf + '_' + yesterday + '.pdf'),stylesheets=["style.css"])

def get():
    get_data(project,query,'booking_nopurchases',path)
    to_pdf('booking_nopurchases','booking_nopurchases')

# if __name__ == '__main__':
#     get()

