import get_data as gd
import message as msg

path = gd.path

def main():
    gd.get()
    msg.sent()

if __name__ == '__main__':
    main()

